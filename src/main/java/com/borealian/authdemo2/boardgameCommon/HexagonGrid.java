package com.borealian.authdemo2.boardgameCommon;

public class HexagonGrid {

    public enum GridOrientation{
        FlatTopOriented,
        AngleTopOriented
    };

    GridOrientation _orientation;

    HexagonTile _root = null;

    public HexagonGrid(GridOrientation orientation, int width, int height){
        _orientation = orientation;
    }

}
